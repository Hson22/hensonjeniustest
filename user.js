var mongoose = require('mongoose');

var redis = require('redis');
var client = redis.createClient();

var userSchema = mongoose.Schema({
    name:{
        type: String
    },
    accountNumber:{
        type: String,
        require:true
    },
    emailAddress: {
        type : String,
    },
    identifyNumber: {
        type : String
    }
});
var User = module.exports =  mongoose.model('users', userSchema);

// get User

module.exports.getUsers = function (callback,limit) {
    User.find(callback).limit(limit);
};

module.exports.getUserById = function(id,callback){
    User.findById(id,callback);
}



module.exports.addNewUser = function(user,callback){
    let newUser = {
        name: user.name,
        emailAddress: user.emailAddress,
        accountNumber: Math.floor(Math.random()*90000) + 10000,
        identifyNumber : user.identifyNumber
    }
    User.create(newUser,callback);
}

module.exports.updateUser = function(id,user,options,callback){
    let query = {_id: id};
    let update = {
        name: user.name,
        emailAddress:user.emailAddress,
        accountNumber: user.accountNumber,
        identifyNumber: user.identifyNumber
    }
    User.findOneAndUpdate(query,update,options,callback);
}
module.exports.findByAccountNumber = function(accountNumber,callback){
    query = {
        accountNumber:accountNumber
    }
    User.find(query,callback);
}

module.exports.findByIndentifyNumber = function(identifyNumber,callback){
    query = {
        identifyNumber:identifyNumber
    }
    User.find(query,callback);
}