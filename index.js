const express = require('express');
const app = express();
const port = 3000
app.use(express.json());
var bodyParser = require('body-parser');
app.use(bodyParser.json());
var mongoose = require('mongoose');
var mongoDB = 'mongodb://localhost/henson';
mongoose.connect(mongoDB, { useNewUrlParser: true });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
User = require('./user');
const url = require('url')
var logger = require('./logger')
var redis = require('redis');
var client = redis.createClient();

app.get('/user',function(req,res){
    User.getUsers(function(err,users){
        if(err) {
            throw err;
        }
        res.json(users);
    });
});
app.get('/user/:id',function(req,res){
    client.get(req.params.id,function(error,result){
       let log = result;
       if(log !== null) {
           res.json(log);
       } else {
        User.getUserById(req.params.id,function(err,users){
            if(err) {
                throw err;
            }
            client.set(users._id,users)
            res.json(users);
        });

       }
    })
});
app.post('/user',function(req,res){
    let user = req.body;
    User.addNewUser(user,function(err,user){
        if(err) {
            throw err;
        }
        res.json(user);
    });
});

app.patch('/user/:id',function(req,res){
    let id = req.params.id;
    let user = req.body;
    User.updateUser(id,user,{},function(err,user){
        if(err) {
            throw err;
        }
        client.set(user._id,user);
        res.json(user);
    });
})

app.get('/search',function(req,res){
    if(req.query.hasOwnProperty('accountNumber')) {
        User.findByAccountNumber(req.query.accountNumber,function(err,users){
            if(err) {
                throw err;
            }
            res.json(users);
        });
    } else if(req.query.hasOwnProperty('identifyNumber')){
        User.findByIndentifyNumber(req.query.identifyNumber,function(err,users){
            if(err) {
                throw err;
            }
            res.json(users);
        });
    } else {
        let response = {
            status:"failed"
        };
        res.json(response);
    }
  
});

// app.patch();
// app.delete();

app.listen(port, () => console.log(`Example app listening on port ${port}!`))